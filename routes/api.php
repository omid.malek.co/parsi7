<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\MailController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('email_send_event',[MailController::class,'emailSendEvent']);
Route::get('sent_mail',[MailController::class,'sendMaill']);
Route::post('register',[AuthController::class,'register']);
Route::post('login',[AuthController::class,'login']);
Route::middleware('auth:api')->group(function (){
    Route::get('get-user',[AuthController::class,'userInfo']);
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
