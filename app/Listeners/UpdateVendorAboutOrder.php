<?php

namespace App\Listeners;

use App\Events\OrderPlaced;
use App\Mail\sendAlert;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
class UpdateVendorAboutOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderPlaced  $event
     * @return void
     */
    public function handle(OrderPlaced $event)
    {
        //--
        $msg='records has change '.$event->text;
        info($msg);
        Mail::send(new sendAlert());
    }
}
